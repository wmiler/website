== BRARS Special Functions ===
Contributors: kj4ctd
Tags: brars
Requires at least: 4.0
Tested up to: 5.2
Stable tag: 0.1.1
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.en.html

Various special functions for the Blue Ridge Amateur Radio Society website

== Description ==

Various special functions for the Blue Ridge Amateur Radio Society website

== Installation ==

Upload the brars.functions plugin to your blog, edit the redirects (coming soon to a form near you) and then Activate it.

== Changelog ==

= 0.1.1 =
* Release Date - 20 June 2019*

* Added brars_set_content_type to make emails html'd.
* Prefixed all of our functions with 'brars_' to prevent name collisions.

= 0.1 =
* Release Date - 8 May 2019*

* Added wp_mail_from and wp_mail_from_name redirects.
* Add notification via email to email lists.
