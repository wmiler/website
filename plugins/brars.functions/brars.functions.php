<?php
/**
 * Plugin Name: BRARS Special Functions
 * Description: Various special functions for the Blue Ridge Amateur Radio Society website
 * Version: 0.1.1
 * Author: Wyatt Miler KJ4CTD
 * Author URI: https://brars.cc/
 * License: GPL3
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Invalid request.' );
}

add_action('publish_post', 'brars_send_notification');

function brars_send_notification($post_ID) {
    $post = get_post($post_ID);

    $postcontents = $post->post_content;
    $title = $post->post_title;
    $permalink = get_permalink( $ID );

    $subject = sprintf( 'Published: %s', $title );
    $message = sprintf (' %s', $postcontents );
    $message .= sprintf( '\n\n' );
    $message .= sprintf( 'View: %s', $permalink );

    wp_mail( 'membership@brars.cc', $subject, $message);
}
/* Various email changes */
add_filter('wp_mail_from','brars_new_mail_from');
add_filter('wp_mail_from_name','brars_new_mail_from_name');
add_filter('wp_mail_content_type','brars_set_content_type');

function brars_new_mail_from($old) {
  return 'brars@brars.cc';
}

function brars_new_mail_from_name($old) {
  return 'Blue Ridge ARS website';
}

function brars_set_content_type() {
	return "text/html";
}
