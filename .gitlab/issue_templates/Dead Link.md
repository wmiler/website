### Summary

(Summarize the dead link encountered concisely)

### Steps to reproduce

(Provide the actual link URI address that failed)

### Expected *correct* behavior

(A clear and concise description of what you expected to happen.)

### Screenshots

(If applicable, add screenshots to help explain your problem.)

### Relevant logs

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's tough to read otherwise.)

### Please tell us what system you are using

 - Device: [e.g. iPhone6]
 - OS: [e.g. iOS]
 - Browser [e.g. chrome, safari]
 - Version [e.g. 22]

### Additional context

(Add any other context about the problem here.)

### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

### For back dating a report

Reporter: 

Date: (mm/dd/yyyy)

/label ~dead_link
