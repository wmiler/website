### Summary

(Summarize the design issue encountered concisely)

### What exactly is the issue and what page do you see it on?

(Provide the actual link URI address that looks wrong)

### Screenshots

(If applicable, add screenshots to help explain your problem.)

### Please tell us what system you are using

 - Device: [e.g. iPhone6]
 - OS: [e.g. iOS]
 - Browser [e.g. chrome, safari]
 - Version [e.g. 22]

### Additional context

(Add any other context about the problem here.)

### Possible fixes

(If you can, suggest what would make the site look better)

### For back dating a report

Reporter: 

Date: (mm/dd/yyyy)

/label ~design
